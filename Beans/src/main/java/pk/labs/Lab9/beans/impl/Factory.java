
package pk.labs.Lab9.beans.impl;
import java.beans.PropertyVetoException;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.io.FileOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;


public class Factory implements java.io.Serializable, ConsultationListFactory{

    public ConsultationList list;
    
    @Override
    public ConsultationList create() {
         return new ConsultationListImpl();
    }

    @Override
    public ConsultationList create(boolean deserialize) {
         Consultation[] result = null;
         ConsultationList lista = new ConsultationListImpl();
         if(deserialize){
        try{
            XMLDecoder decoder = new XMLDecoder(
                    new BufferedInputStream(
                    new FileInputStream("plik.xml")));
            
            result = (Consultation[])decoder.readObject();
            decoder.close();
            for(int i=0; i<result.length;i++)
            {
                lista.addConsultation(result[i]);
            }
            }
        catch(FileNotFoundException e) {
            System.out.print(e.getMessage());
        }    catch (PropertyVetoException ex) {   
                 Logger.getLogger(Factory.class.getName()).log(Level.SEVERE, null, ex);
             }   
        }
         
        return lista;
    }

    @Override
    public void save(ConsultationList consultationList) {
      try{
            XMLEncoder encoder = new XMLEncoder(
                    new BufferedOutputStream(
                    new FileOutputStream("plik.xml")));

            encoder.writeObject(consultationList.getConsultation());
            encoder.close();
        }
        catch(FileNotFoundException e){
            System.out.print(e.getMessage());
        }
        }
    
    
}