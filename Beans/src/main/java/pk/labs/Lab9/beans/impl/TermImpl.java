package pk.labs.Lab9.beans.impl;

import java.util.Date;
import pk.labs.Lab9.beans.Term;

public class TermImpl  implements Term{
    private Date begin;
    private int duration;
    public TermImpl()
    {
    this.begin = new Date();
    }
    @Override
    public Date getBegin()
    {
        return this.begin;
    }

   @Override
   public void setBegin(Date begin)
   {
       this.begin = begin;
   }
    
   @Override
   public int getDuration()
       {
       return this.duration;
       }
   
    @Override
    public void setDuration(int duration)
        {
               if(duration > 0) 
               {
           this.duration = duration;
        }
            
        }
     @Override
      public  Date getEnd()
      {
     return new Date(this.begin.getTime() + this.duration*60000);
     }
              
}
