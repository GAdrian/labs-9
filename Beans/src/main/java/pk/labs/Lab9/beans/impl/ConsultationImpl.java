
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;
import java.util.Date;
import pk.labs.Lab9.beans.Term;
import pk.labs.Lab9.beans.Consultation;

public class ConsultationImpl implements Consultation {
    
    
    
    private final VetoableChangeSupport vcs = new VetoableChangeSupport(this);
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private Term term;
    private String student;
    public ConsultationImpl()
    {
        
    }
    
    @Override
    public String getStudent() {
       return this.student;
    }

    @Override
    public void setStudent(String student) {
       this.student = student;
    }

    @Override
    public Date getBeginDate() {
       return this.term.getBegin();
    }

    @Override
    public Date getEndDate() {
       return this.term.getEnd();
    }

    @Override
    public void setTerm(Term term) throws PropertyVetoException {
       Term old = this.term;
       vcs.fireVetoableChange("Term", old, term);
       this.term = term;
       pcs.firePropertyChange("Term", old, term);
    }
    
    public Term getTerm()
    {
        return this.term;
    }

    @Override
    public void prolong(int minutes) throws PropertyVetoException
    {
        if (minutes <0) {
            minutes = 0;
        }
        int oldM = this.term.getDuration();
        vcs.fireVetoableChange("Term", oldM, oldM + minutes);
        this.term.setDuration(this.term.getDuration() + minutes);
        pcs.firePropertyChange("Term", oldM, oldM + minutes);
    }
}

