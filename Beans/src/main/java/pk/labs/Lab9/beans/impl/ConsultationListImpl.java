
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.util.Arrays;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.Consultation;

public class ConsultationListImpl implements ConsultationList {
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private Consultation[] consultationlist;
    
    public ConsultationListImpl()
    {
        this.consultationlist = new Consultation[]{};
    }
    @Override
    public int getSize() {
       return this.consultationlist.length;
    }

    @Override
    public Consultation[] getConsultation() {
       return this.consultationlist;
    }

    @Override
    public Consultation getConsultation(int index) {
       return this.consultationlist[index];
    }

    @Override
    public void addConsultation(Consultation consultation) throws PropertyVetoException {
        for(int i=0; i < this.consultationlist.length;i++)
        {
            if(consultation.getBeginDate().before(this.consultationlist[i].getBeginDate())&&(consultation.getEndDate().after(this.consultationlist[i].getBeginDate()))
                    ||(consultation.getBeginDate().before(this.consultationlist[i].getEndDate()) && consultation.getEndDate().after(this.consultationlist[i].getBeginDate())))
            {
                throw new PropertyVetoException("error", null);
            }          
        }   
        Consultation[] neW = Arrays.copyOf(this.consultationlist,  this.consultationlist.length + 1);
        Consultation[] olD = this.consultationlist;
        neW[neW.length - 1] = consultation;        
        this.consultationlist = neW;       
        pcs.firePropertyChange("consultation", olD, neW);
        
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
       this.pcs.addPropertyChangeListener(listener);
    }

    
    
}
